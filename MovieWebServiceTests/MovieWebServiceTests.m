//
//  MovieWebServiceTests.m
//  MovieWebServiceTests
//
//  Created by testDev on 4/11/17.
//  Copyright © 2017 TestCompany. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DBManager.h"
#import "MoviesListInteractor.h"

@interface MovieWebServiceTests : XCTestCase
@property (nonatomic, strong) id mockInteractor;
@property (nonatomic, strong) id mockManager;
@end

@implementation MovieWebServiceTests

- (void)setUp {
    [super setUp];
    
    self.mockInteractor = [[MoviesListInteractor alloc] init];
    self.mockManager = OCMPartialMock([self.mockInteractor getDBManager]);
}

- (void)testDBManagerFromInteractor {
    OCMExpect([self.mockManager testJsonMethod]);
    
    [self.mockInteractor anyTestProtocolMethod];
    
    [self.mockManager verify];
    [self.mockManager stopMocking];
}

//  https://stackoverflow.com/questions/41275442/testing-void-methods-using-ocmock-in-ios
- (void)testDBManagerSingleton {
    MoviesListInteractor *interactor = [[MoviesListInteractor alloc] init];
    id mockForStubbingClassMethod = OCMClassMock([DBManager class]);
    DBManager *baseForPartialMock = [[DBManager alloc] init];
    
    // ~~~
    id mockObject = OCMPartialMock(baseForPartialMock);
    OCMStub([mockForStubbingClassMethod crazyInitializer]).andReturn(mockObject);
    OCMExpect([mockObject testJsonMethod]);
    [interactor anyTestProtocolMethod];
    
    [self.mockManager verify];
    [self.mockManager stopMocking];
}

- (void)tearDown {
    [super tearDown];
}

@end
