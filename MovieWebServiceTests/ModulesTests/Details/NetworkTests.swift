//
//  12.swift
//  MovieWebService
//
//  Created by Vitaliy Zarubin on 04.11.17.
//  Copyright © 2017 TestCompany. All rights reserved.
//

import XCTest
import OCMock
import Quick
import Nimble

@testable
import MovieWebService

class NetworkEngineMock: NetworkEngine {
    typealias Handler = NetworkEngine.Handler
    
    var requestedURL: URL?
    
    func performRequest(for url: URL, completionHandler: @escaping Handler) {
        requestedURL = url
        
        let data = "Hello world".data(using: .utf8)
        completionHandler(data, nil, nil)
    }
}

class NetworkTestsSpec: QuickSpec {
    
    override func spec() {
        
        describe("When data loader work with network engine") {
            var engine: NetworkEngineMock!
            var loader: DataLoader!
            beforeEach {
                engine = NetworkEngineMock()
                loader = DataLoader(engine: engine)
            }
            
            context("Data loader") {
                it("works fine") {
                    var result: DataLoader.Result?
                    let url = URL(string: "my/API")!
                    loader.load(from: url) { result = $0 }
                    
                    XCTAssertEqual(engine.requestedURL, url)
                    expect(result?.getString()).to(equal("Hello world"))
                }
            }
            
            afterEach {
                engine = nil
                loader = nil
            }
        }
        
    }
}
