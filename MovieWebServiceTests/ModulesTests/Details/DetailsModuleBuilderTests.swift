//
//  DetailsModuleBuilderTests.swift
//  MovieWebService
//
//  Created by testDev on 11/04/2017.
//  Copyright © 2017 Agoda Services Co. Ltd. All rights reserved.
//

import XCTest
import OCMock
import Quick
import Nimble

@testable
import MovieWebService

class DetailsModuleBuilderTests: XCTestCase {

    override func setUp() {
        super.setUp()
        
    }
    
    override func tearDown() {
        
        super.tearDown()
    }
    
    func testBuildViewController() {

        // given
        let builder = MovieDetailsAssembly()

        // when
        let viewController = builder.build(with: Film()) as! DetailsViewController

        // then
        XCTAssertNotNil(viewController.presenter)
        XCTAssertTrue(viewController.presenter != nil)

        let presenter: DetailsPresenter = viewController.presenter
        XCTAssertNotNil(presenter.view)
    }

}

