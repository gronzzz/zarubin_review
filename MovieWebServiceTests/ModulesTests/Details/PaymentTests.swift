//
//  PaymentTests.swift
//  MovieWebService
//
//  Created by Vitaliy Zarubin on 05.11.17.
//  Copyright © 2017 TestCompany. All rights reserved.
//

import XCTest
import OCMock
import Quick
import Nimble

@testable
import MovieWebService

class PaymentTestsSpec: QuickSpec {
    
    override func spec() {
        super.spec()
        
        describe("Payment") {
            beforeEach {
            }
            
            it("Equals enum types") {
                let payment = PaymentType.cash
                expect({
                    guard case .cash = payment else {
                        return .failed(reason: "wrong enum case")
                    }
                    
                    return .succeeded
                }).to(succeed())
            }
            
            it("Do not fails on PayPal") {
                let payment = PaymentType.payPal(transactionId: "id", transactionAuthCode: nil)
                if case .cash = payment {
                    fail("Only I appear")
                }
            }
            
            afterEach {
            }
        }
        
    }
}
