//
//  DetailsViewTests.swift
//  MovieWebService
//
//  Created by testDev on 11/04/2017.
//  Copyright © 2017 Agoda Services Co. Ltd. All rights reserved.
//

import XCTest
import OCMock

@testable
import MovieWebService
import Quick
import Nimble

class DetailsViewTestsSpec: QuickSpec {
    override func spec() {
        describe("Details VC") {
            var detailsVC: DetailsViewController!
            beforeEach {
                detailsVC = DetailsViewController()
            }
            
            context("Button") {
                it("Shown on start") {
                    expect(detailsVC.showMoreButton.isHidden).to(beFalse())
                }
                
                it("hidden after touch") {
                    // When
                    detailsVC.showMoreAction()
                    // Then
                    expect(detailsVC.showMoreButton.isEnabled).to(beTrue())
                }
            }
            
            afterEach {
                detailsVC = nil
            }
        }
        
        describe("Films") {
            var detailsViewInput: Any!
            beforeEach {
//                detailsViewInput = OCMockObject.niceMock(for: DetailsViewInput)
//                detailsViewInput = OCMProtocolMock(@protocol(DetailsViewInput));
            }
            context("") {
                it("Did refresh") {
//                didRefreshFilm   
                }
            }
            
            afterEach {
                detailsViewInput = nil
            }
        }
    }
}
