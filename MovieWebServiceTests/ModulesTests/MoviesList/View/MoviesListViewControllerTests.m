//
//  MoviesListViewControllerTests.m
//  MovieWebService
//
//  Created by testDev on 11/04/2017.
//  Copyright © 2017 Agoda Services Co. Ltd. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "MoviesListViewController.h"

#import "MoviesListView.h"
#import "MoviesListInteractor.h"
#import "DBManager.h"
#import "Film.h"

@import Nimble;
@import Quick;

@interface MoviesListViewControllerTests : XCTestCase

@property (nonatomic, strong) MoviesListViewController *controller;

@property (nonatomic, strong) id mockPresenterOutput;
@end


@implementation MoviesListViewControllerTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];

    self.controller = [[MoviesListViewController alloc] init];
    self.controller.presenter = OCMClassMock([MoviesListPresenter class]);
}

- (void)tearDown {
    self.controller = nil;

    [super tearDown];
}

#pragma mark - Тестирование жизненного цикла

- (void)testThatPresenterNotifiesController {
	
    // given
    OCMExpect([self.controller.presenter didLoadFilms:[OCMArg isNotNil]]);
    OCMExpect([self.controller.presenter didLoadFilms:[OCMArg isKindOfClass:[NSArray class]]]);
    
	// when
    OCMExpect([self.controller.presenter updateView]);
    
	// then
    OCMVerify([self.controller didRefreshFilms]);
}

- (void)testModel_NotNil {
    Film *film = [[Film alloc] initWithDictionary:[DBManager json]];
    XCTAssertNotNil(film, @"Parse film not successfull");
}

@end

