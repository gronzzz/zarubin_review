//
//  MoviesListInteractorTests.m
//  MovieWebService
//
//  Created by testDev on 11/04/2017.
//  Copyright © 2017 Agoda Services Co. Ltd. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "MoviesListInteractor.h"

#import "MoviesListInteractorIO.h"
#import "DBManager.h"
#import "Film.h"

@import Nimble;
@import Quick;


QuickSpecBegin(MoviesListInteractorTests)

qck_describe(@"JSON for DB", ^{
    
    __block MoviesListInteractor *interactor;
    __block id mockOutput;
    
    __block NSDictionary *json;
    qck_beforeEach(^{
        interactor = [[MoviesListInteractor alloc] init];
        mockOutput = OCMProtocolMock(@protocol(MoviesListInteractorOutput));
        
        DBManager *mockMan = [OCMockObject niceMockForClass:[DBManager class]];
        json = [DBManager json];
    });

    qck_context(@"", ^{
        qck_it(@"Check dictionary", ^{
            expect(json).toNot(beNil());
        });
    });
    
    qck_context(@"has right params", ^{
        qck_it(@"nominated", ^{
            NSInteger isNominated = [json[@"nominated"] integerValue];
            expect(isNominated).to(beTruthy());
        });
        
        qck_it(@"rating", ^{
            int filmRatingInt = [json[@"filmRating"] intValue];
            expect(filmRatingInt).to(beLessThanOrEqualTo(FilmRatingUnknown));
        });
    });
    
    qck_afterEach(^{
        interactor = nil;
        mockOutput = nil;
        json = nil;
    });
});

qck_describe(@"Film", ^{
    __block Film *film;
    qck_beforeEach(^{
        film = [[Film alloc] initWithDictionary:[DBManager json]];
    });
    
    qck_context(@"download from db", ^{
        qck_it(@"", ^{
            waitUntil(^(void (^done)(void)){
                [[DBManager sharedInstance] getFilmWithCallback:^(Film *film) {
                    expect(film).toNot(beNil());
                    done();
                }];
            });
        });
    });
    
    qck_context(@"Set completely", ^{
        qck_it(@"", ^{
            FilmRating fr = [Film filmRatingFromNumber:@9];
            expect((int)(fr)).to(equal(FilmRatingUnknown));
        });
    });
});

QuickSpecEnd






