//
//  MoviesListPresenterTests.m
//  MovieWebService
//
//  Created by testDev on 11/04/2017.
//  Copyright © 2017 Agoda Services Co. Ltd. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "MoviesListPresenter.h"
#import "MoviesListInteractor.h"

#import "MoviesListView.h"
#import "MoviesListInteractorIO.h"
#import "MoviesListRouterInput.h"
#import "Film.h"
#import "Actor.h"
#import "DBManager.h"


@import Quick;
@import Nimble;

QuickSpecBegin(MoviesListPresenterTests2)

qck_describe(@"film", ^{
    __block Film *film = nil;
    __block Film *emptyFilm = nil;
    qck_beforeEach(^{
        film = [[Film alloc] initWithDictionary:[DBManager json]];
        emptyFilm = [[Film alloc] initWithDictionary:nil];
    });
    
    qck_beforeEachWithMetadata(^(ExampleMetadata *exampleMetadata) {
        NSLog(@"Example number %li is about to be run.", exampleMetadata.exampleIndex);
    });
    
    qck_context(@"film on board", ^{
        qck_it(@"exist", ^{
            expect(film).toNot(beNil());
            expect(film.director).toNot(beNil());
            expect(film.name.length).to(beGreaterThan(@0));
        });
    });
    
    qck_context(@"Film with null dict", ^{
        qck_it(@"exists", ^{
            expect(emptyFilm).toNot(beNil());
        });
    });
    
    qck_it(@"Check actor", ^{
        // Given
        Actor *anActorToTest = [[Actor alloc] init];
        
        // When
        id mockPersonToTest = [OCMockObject partialMockForObject:anActorToTest];
        [[[mockPersonToTest stub] andReturn:@"George Bush"] screenName];
        [[[mockPersonToTest stub] andReturn:@"George Bush"] name];
        
        // Then
        expect([mockPersonToTest screenName]).to(match([mockPersonToTest name]));
    });
    
    qck_afterEachWithMetadata(^(ExampleMetadata *exampleMetadata){
        NSLog(@"Example number %li has run.", exampleMetadata.exampleIndex);
    });
    
    qck_afterEach(^{
        film = nil;
    });
});

qck_describe(@"", ^{
    qck_beforeEach(^{
    });
    
    qck_context(@"Interactor", ^{
        qck_xit(@"First part", ^{
        });
        
        qck_it(@"Second part", ^{
        });
        
        qck_xit(@"should verify request to DB", ^{
        });
    });
    
    qck_afterEach(^{
    });
});

QuickSpecEnd


@interface MoviesListPresenterTests : QuickSpec

@property (nonatomic, strong) MoviesListPresenter *presenter;

@property (nonatomic, strong) id mockInteractor;
@property (nonatomic, strong) id mockRouter;
@property (nonatomic, strong) id mockView;

@property (nonatomic, strong) Film *film;
@end


@implementation MoviesListPresenterTests

#pragma mark - Настройка окружения для тестирования

- (void)setUp {
    [super setUp];
    
    self.mockView = OCMProtocolMock(@protocol(MoviesListViewOutput));
    self.mockInteractor = OCMProtocolMock(@protocol(MoviesListInteractorInput));
    self.mockRouter = OCMProtocolMock(@protocol(MoviesListRouterInput));
    
    self.presenter = [[MoviesListPresenter alloc] initWithInterface:self.mockView
                                                         interactor:self.mockInteractor
                                                             router:self.mockRouter];
    
}

- (void)tearDown {
    self.presenter = nil;
    
    self.mockView = nil;
    self.mockRouter = nil;
    self.mockInteractor = nil;
    
    [super tearDown];
}

#pragma mark - Тестирование методов

- (void)testCheckGetFilmsExpectation {
    [[[self.mockInteractor expect] andDo:^(NSInvocation *invocation) {
    }] getFilms];
    
    [self.mockView verify];
}

@end
