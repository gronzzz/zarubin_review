//
//  NetworkSession.swift
//  MovieWebService
//
//  Created by Vitaliy Zarubin on 04.11.17.
//  Copyright © 2017 TestCompany. All rights reserved.
//

import UIKit

protocol NetworkEngine {
    typealias Handler = (Data?, URLResponse?, Error?) -> Void
    
    func performRequest(for url: URL, completionHandler: @escaping Handler)
}

extension URLSession: NetworkEngine {
    typealias Handler = NetworkEngine.Handler
    
    func performRequest(for url: URL, completionHandler: @escaping Handler) {
        let task = dataTask(with: url, completionHandler: completionHandler)
        task.resume()
    }
}
