//
//  DBManager+ManualJSON.m
//  MovieWebService
//
//  Created by Vitaliy Zarubin on 08.11.17.
//  Copyright © 2017 TestCompany. All rights reserved.
//

#import "DBManager+ManualJSON.h"

#import "Actor.h"
#import "Film.h"

@implementation DBManager (ManualJSON)

+ (NSDictionary *)json {
    NSDictionary *dictionary = @{
                                 kName: @"Argo",
                                 kFilmRating: @3,
                                 kLanguages: @[
                                         @"English",
                                         @"Thai"
                                         ],
                                 kNominated: @1,
                                 kReleaseDate: @1350000000,
                                 kCast: @[
                                         @{
                                             kDateOfBirth: @-436147200,
                                             kNominated: @1,
                                             kName: @"Bryan Cranston",
                                             kScreenName: @"Jack Donnell",
                                             kBiography: @"Bryan Lee Cranston is an American actor, voice actor, writer and director."
                                             }
                                         ],
                                 kMark: @7.8,
                                 kDirector: @{
                                         kDateOfBirth: @82684800,
                                         kNominated: @1,
                                         kName: @"Ben Affleck",
                                         kBiography: @"Benjamin Geza Affleck was born on August 15, 1972 in Berkeley, California, USA but raised in Cambridge, Massachusetts, USA."
                                         }
                                 };
    
    return dictionary;
}


@end
