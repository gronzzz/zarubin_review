//
//  DataLoader.swift
//  MovieWebService
//
//  Created by Vitaliy Zarubin on 04.11.17.
//  Copyright © 2017 TestCompany. All rights reserved.
//

import UIKit

final class DataLoader {
    enum Result {
        case data(Data)
        case error(Error)
        
        func getString() -> String {
            switch self {
            case .data(let data):
                return String.init(data: data, encoding: .utf8)!
            case .error(let error):
                return error.localizedDescription
            }
        }
    }
    
    private let engine: NetworkEngine
    
    init(engine: NetworkEngine = URLSession.shared) {
        self.engine = engine
    }
    
    func load(from url: URL, completionHandler: @escaping (Result) -> Void) {
        engine.performRequest(for: url) { (data, response, error) in
            if let error = error {
                return completionHandler(.error(error))
            }
            
            completionHandler(.data(data ?? Data()))
        }
    }
}
