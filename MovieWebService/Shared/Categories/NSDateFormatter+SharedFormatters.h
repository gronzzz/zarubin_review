//
//  NSDateFormatter+SharedFormatter.h
//  MovieWebService
//
//  Created by Vitaliy Zarubin on 14.10.17.
//  Copyright © 2017 TestCompany. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDateFormatter (SharedFormatters)

/**
 Simple NSDateFormatter formatter without params
 */
+ (NSDateFormatter *)simpleDateTimeFormatter;

@end
