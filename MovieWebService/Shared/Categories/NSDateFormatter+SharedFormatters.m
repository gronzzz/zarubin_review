//
//  NSDateFormatter+SharedFormatter.m
//  MovieWebService
//
//  Created by Vitaliy Zarubin on 14.10.17.
//  Copyright © 2017 TestCompany. All rights reserved.
//

#import "NSDateFormatter+SharedFormatters.h"

@implementation NSDateFormatter (SharedFormatters)

+ (NSDateFormatter *)simpleDateTimeFormatter {
    static NSDateFormatter *_simpleDateTimeFormatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _simpleDateTimeFormatter = [[NSDateFormatter alloc] init];
        [_simpleDateTimeFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        [_simpleDateTimeFormatter setLocale:[NSLocale currentLocale]];
        [_simpleDateTimeFormatter setDateFormat:@"MMMM d, yyyy"];
    });
    
    return _simpleDateTimeFormatter;
}

@end
