//
//  AssemblyBuilder.h
//  MovieWebService
//
//  Created by Vitaliy Zarubin on 10.11.17.
//  Copyright © 2017 TestCompany. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Film;
@interface AssemblyBuilder : NSObject

+ (UIViewController *)viewForMoviesList;
+ (UIViewController *)viewForMovieDetailsWithFilm:(Film *)film;

- (instancetype) init __attribute__((unavailable("init not available")));
@end
