//
//  DBManager.h
//  MovieWebService
//
//  Created by Vitaliy Zarubin on 17.10.17.
//  Copyright © 2017 TestCompany. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Film;
@interface DBManager : NSObject

+ (instancetype)sharedInstance;
- (DBManager *)crazyInitializer;

- (void)testJsonMethod;
- (void)getFilmWithCallback:(void (^)(Film *film))callback;

@end
