//
//  DBManager.m
//  MovieWebService
//
//  Created by Vitaliy Zarubin on 17.10.17.
//  Copyright © 2017 TestCompany. All rights reserved.
//

#import "DBManager.h"
#import "DBManager+ManualJSON.h"
#import "Film.h"

@implementation DBManager

+ (instancetype)sharedInstance {
    static DBManager *_dbManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _dbManager = [[self alloc] init];
    });
    
    return _dbManager;
}

- (DBManager *)crazyInitializer {
    return [[DBManager alloc] init];
}

- (void)getFilmWithCallback:(void (^)(Film *film))callback {
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_UTILITY, 0), ^{
        
        Film *film = [[Film alloc] initWithDictionary:[DBManager json]];
        if (callback) {
            callback(film);
        }
    });
}

- (void)testJsonMethod {
    NSLog(@"%@", self);
}

@end
