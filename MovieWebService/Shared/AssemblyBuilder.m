//
//  AssemblyBuilder.m
//  MovieWebService
//
//  Created by Vitaliy Zarubin on 10.11.17.
//  Copyright © 2017 TestCompany. All rights reserved.
//

#import "AssemblyBuilder.h"

#import "MoviesListAssembly.h"
#import "MovieWebService-Swift.h"
#import "Film.h"

@implementation AssemblyBuilder

+ (UIViewController *)viewForMoviesList {
    return [MoviesListAssembly build];
}

+ (UIViewController *)viewForMovieDetailsWithFilm:(Film *)film {
    return [MovieDetailsAssembly buildWith:film];
}

@end
