//
//  PaymentTypes.swift
//  MovieWebService
//
//  Created by Vitaliy Zarubin on 05.11.17.
//  Copyright © 2017 TestCompany. All rights reserved.
//

import Foundation

enum PaymentType {
    case cash
    case card(
        number: String,
        holderName: String,
        expirationDate: String
    )
    case giftCertificate(
        number: String
    )
    case payPal(
        transactionId: String,
        transactionAuthCode: String?
    )
}
