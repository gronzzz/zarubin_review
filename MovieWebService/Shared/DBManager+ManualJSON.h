//
//  DBManager+ManualJSON.h
//  MovieWebService
//
//  Created by Vitaliy Zarubin on 08.11.17.
//  Copyright © 2017 TestCompany. All rights reserved.
//

#import "DBManager.h"

@interface DBManager (ManualJSON)

+ (NSDictionary *)json;

@end
