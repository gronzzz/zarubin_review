//
//  Film.h
//  MovieWebService
//
//  Created by testDev on 4/11/17.
//  Copyright © 2017 TestCompany. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

FOUNDATION_EXPORT NSString *const kMark;
FOUNDATION_EXPORT NSString *const kFilmRating;
FOUNDATION_EXPORT NSString *const kLanguages;
FOUNDATION_EXPORT NSString *const kReleaseDate;
FOUNDATION_EXPORT NSString *const kCast;
FOUNDATION_EXPORT NSString *const kDirector;

typedef NS_ENUM(NSInteger, FilmRating) {
    FilmRatingG = 0,
    FilmRatingPG,
    FilmRatingPG13,
    FilmRatingR,
    FilmRatingNC17,
    FilmRatingUnknown
};

@class Actor, Director;

@interface Film : NSObject

@property (nonatomic, strong, readonly) NSString *name;
@property (nonatomic, assign, readonly) double mark;
@property (nonatomic, strong, readonly) Director *director;

@property (nonatomic, assign, readonly) FilmRating filmRating;
@property (nonatomic, copy, readonly) NSString *filmRatingName;

@property (nonatomic, assign, readonly) BOOL nominated;

@property (nonatomic, strong, readonly) NSArray *languages;
@property (nonatomic, strong, readonly) NSDate *releaseDate;
@property (nonatomic, strong, readonly) NSArray<Actor*> *cast;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

#pragma mark - Class method
+ (FilmRating)filmRatingFromNumber:(NSNumber *)number;

@end

NS_ASSUME_NONNULL_END
