//
//  GenericRole.m
//  MovieWebService
//
//  Created by testDev on 4/11/17.
//  Copyright © 2017 TestCompany. All rights reserved.
//

#import "GenericRole.h"
#import "Film.h"

NSString *const kName = @"name";
NSString *const kBiography = @"biography";
NSString *const kDateOfBirth = @"dateOfBirth";
NSString *const kNominated = @"nominated";

@interface GenericRole()
@property (nonatomic, strong, readwrite) NSString *name;
@property (nonatomic, strong, readwrite) NSString *biography;
@property (nonatomic, strong, readwrite) NSDate *dateOfBirth;
@property (nonatomic, assign, getter=isNominated, readwrite) BOOL nominated;
@end

@implementation GenericRole

#pragma mark - Lifecycle

- (instancetype)initWithDictionary:(NSDictionary *)dictionary {
    if (self) {
        _name = dictionary[kName];
        _biography = dictionary[kBiography];
        _dateOfBirth = [NSDate dateWithTimeIntervalSince1970:[dictionary[kDateOfBirth] doubleValue]];
        _nominated = [dictionary[kNominated] boolValue];
    }
    
    return self;
}

@end
