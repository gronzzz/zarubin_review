//
//  GenericRole.h
//  MovieWebService
//
//  Created by testDev on 4/11/17.
//  Copyright © 2017 TestCompany. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Film;

FOUNDATION_EXPORT NSString *const kName;
FOUNDATION_EXPORT NSString *const kBiography;
FOUNDATION_EXPORT NSString *const kDateOfBirth;
FOUNDATION_EXPORT NSString *const kNominated;

@interface GenericRole : NSObject

@property (nonatomic, strong, readonly) NSString *name;
@property (nonatomic, strong, readonly) NSString *biography;
@property (nonatomic, strong, readonly) NSDate *dateOfBirth;
@property (nonatomic, assign, getter=isNominated, readonly) BOOL nominated;

@property (nonatomic, weak) Film *film;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
