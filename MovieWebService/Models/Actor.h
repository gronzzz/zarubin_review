//
//  Actor.h
//  MovieWebService
//
//  Created by testDev on 4/11/17.
//  Copyright © 2017 TestCompany. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GenericRole.h"

FOUNDATION_EXPORT NSString *const kScreenName;

@interface Actor : GenericRole

@property (nonatomic, copy, readonly) NSString *screenName;

+ (instancetype)actorWithDictionary:(NSDictionary *)dictionary;

@end
