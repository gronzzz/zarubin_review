//
//  Film.m
//  MovieWebService
//
//  Created by testDev on 4/11/17.
//  Copyright © 2017 TestCompany. All rights reserved.
//

#import "Film.h"
#import "Actor.h"
#import "Director.h"

NSString *const kMark = @"mark";
NSString *const kFilmRating = @"filmRating";
NSString *const kLanguages = @"languages";
NSString *const kReleaseDate = @"releaseDate";
NSString *const kCast = @"cast";
NSString *const kDirector = @"director";

NSString *const kFilmRatingG = @"G";
NSString *const kFilmRatingPG = @"PG";
NSString *const kFilmRatingPG13 = @"PG13";
NSString *const kFilmRatingR = @"R";
NSString *const kFilmRatingNC17 = @"NC17";

@interface Film()

@property (nonatomic, strong, readwrite) NSString *name;
@property (nonatomic, assign, readwrite) double mark;
@property (nonatomic, strong, readwrite) Director *director;

@property (nonatomic, assign, readwrite) FilmRating filmRating;
@property (nonatomic, copy, readwrite) NSString *filmRatingName;

@property (nonatomic, assign, readwrite) BOOL nominated;

@property (nonatomic, strong, readwrite) NSArray *languages;
@property (nonatomic, strong, readwrite) NSDate *releaseDate;
@property (nonatomic, strong, readwrite) NSArray<Actor*> *cast;


@end

@implementation Film

#pragma mark - Lifecycle

- (instancetype)initWithDictionary:(NSDictionary *)dictionary {
    self = [super init];
    if (self) {
        _name = dictionary[kName];
        _mark = [dictionary [kMark] doubleValue];
        
        _filmRating = [Film filmRatingFromNumber:dictionary[kFilmRating]];
        _nominated = [dictionary[kNominated] boolValue];
        _languages = [NSArray arrayWithArray:dictionary[kLanguages]];
        _releaseDate = [NSDate dateWithTimeIntervalSince1970:[dictionary[kReleaseDate] doubleValue]];

        NSArray *castsData = dictionary[kCast];
        NSMutableArray *castList = [NSMutableArray arrayWithCapacity:castsData.count];
        for (NSDictionary *castDictionary in castsData) {
            Actor *actor = [Actor actorWithDictionary:castDictionary];
            actor.film = self;
            [castList addObject:actor];
        }
        _cast = castList;
        
        _director = [Director directorWithDictionary:dictionary[kDirector]];
        _director.film = self;
    }
    
    return self;
}

#pragma mark - Setter/getter
- (void)setName:(NSString *)name {
    if (_name != name) {
        _name = name;
    }
}

- (NSString *)filmRatingName {
    NSString *filmRatingName = @"";
    switch (self.filmRating) {
        case FilmRatingG:
            filmRatingName = kFilmRatingG;
            break;
            
        case FilmRatingPG:
            filmRatingName = kFilmRatingPG;
            break;
            
        case FilmRatingPG13:
            filmRatingName = kFilmRatingPG13;
            break;
            
        case FilmRatingR:
            filmRatingName = kFilmRatingR;
            break;
            
        case FilmRatingNC17:
            filmRatingName = kFilmRatingNC17;
            break;
            
        default:
            break;
    }
    
    return filmRatingName;
}

#pragma mark - Class method
+ (FilmRating)filmRatingFromNumber:(NSNumber *)number {
    if ([number intValue] > FilmRatingUnknown) {
        return FilmRatingUnknown;
    }
    
    return (FilmRating)[number intValue];
}

@end
