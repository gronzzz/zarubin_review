//
//  Actor.m
//  MovieWebService
//
//  Created by testDev on 4/11/17.
//  Copyright © 2017 TestCompany. All rights reserved.
//

#import "Actor.h"
#import "GenericRole.h"

NSString *const kScreenName = @"screenName";

@interface Actor()
@property (nonatomic, copy, readwrite) NSString *screenName;
@end

@implementation Actor

#pragma mark – Lifecycle
+ (instancetype)actorWithDictionary:(NSDictionary *)dictionary {
    return [[self alloc] initWithDictionary:dictionary];
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionary {
    self = [super initWithDictionary:dictionary];
    if (self) {
        _screenName = dictionary[kScreenName];
    }
    return self;
}

@end
