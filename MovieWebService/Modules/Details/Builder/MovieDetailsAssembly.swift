//
//  MovieDetailsAssembly.swift
//  MovieWebService
//
//  Created by testDev on 11/04/2017.
//  Copyright © 2017 Agoda Services Co. Ltd. All rights reserved.
//

import UIKit

@objc class MovieDetailsAssembly: NSObject {

    class func build(with film: Film) -> UIViewController {

        let viewController = DetailsViewController()

        let presenter = DetailsPresenter()
        presenter.view = viewController
        presenter.setFilm(film: film)
        
        viewController.presenter = presenter

        return viewController
    }

    @available(*, unavailable, message: "init is unavailable, use initWithFrame")
    override init() { }
}
