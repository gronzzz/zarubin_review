//
//  DetailsPresenter.swift
//  MovieWebService
//
//  Created by testDev on 11/04/2017.
//  Copyright © 2017 Agoda Services Co. Ltd. All rights reserved.
//

final class DetailsPresenter {
    
    weak var view: DetailsViewInput?
    
    var film: Film!
}

extension DetailsPresenter: DetailsPresenterInput {
    func updateView() {
        filmDidLoad(film: self.film)
    }
    
    func filmDidLoad(film: Film) {
        self.view?.didRefreshFilm()
    }
}

// MARK: - RouteModuleInput
extension DetailsPresenter: DetailsModuleInput {
    
    func setFilm(film: Film) {
        self.film = film
    }
}

