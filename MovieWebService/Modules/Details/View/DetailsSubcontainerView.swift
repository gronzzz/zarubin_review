//
//  DetailsSubcontainerView.swift
//  MovieWebService
//
//  Created by Vitaliy Zarubin on 16.10.17.
//  Copyright © 2017 TestCompany. All rights reserved.
//

import Foundation
import UIKit
import Masonry

final class DetailsSubcontainerView: UIView {

    // MARK: - Public params
    var title: String? {
        get {
            return titleLabel.text
        }
        set {
            titleLabel.text = newValue
        }
    }
    
    var text: String? {
        get {
            return textLabel.text
        }
        set {
            textLabel.text = newValue
            setNeedsLayout()
        }
    }
    
    // MARK: - Private
    fileprivate lazy var titleLabel: UILabel = {
        var label = UILabel()
        label.font = label.font.withSize(20)
        label.numberOfLines = 1
        
        return label
    }()
    
    private lazy var textLabel: UILabel = {
        var label = UILabel()
        label.font = label.font.withSize(14)
        label.numberOfLines = 0
                
        return label
    }()
    
    // MARK: – Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(titleLabel)
        self.addSubview(textLabel)
        
        titleLabel.mas_makeConstraints { (make:MASConstraintMaker?) in
            make?.top.equalTo()(self.mas_top)?.with().offset()(8)
            make?.left.equalTo()(self.mas_left)?.with().offset()(16)
            make?.right.equalTo()(self.mas_right)?.with().offset()(-16)
            make?.height.equalTo()(25)?.with().priority()(750)
        }
        
        textLabel.mas_makeConstraints { (make:MASConstraintMaker?) in
            make?.top.equalTo()(titleLabel.mas_bottom)?.with().offset()(0)
            make?.left.equalTo()(titleLabel.mas_left)
            make?.right.equalTo()(titleLabel.mas_right)
            make?.bottom.equalTo()(self.mas_bottom)?.with().offset()(8)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
