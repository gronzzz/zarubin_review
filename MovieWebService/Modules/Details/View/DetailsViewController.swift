//
//  DetailsViewController.swift
//  MovieWebService
//
//  Created by testDev on 11/04/2017.
//  Copyright © 2017 Agoda Services Co. Ltd. All rights reserved.
//

import UIKit
import Masonry

final class DetailsViewController: UIViewController {

    public var presenter: DetailsPresenter!
    
    lazy public var showMoreButton: UIButton! = {
        let button = UIButton(type: .system)
        let buttonTitle = NSLocalizedString("Tap here to show more", comment: "Show more button")
        button.setTitle(buttonTitle, for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.contentHorizontalAlignment = .left
        button.backgroundColor = .clear
        button.addTarget(self, action: #selector(DetailsViewController.showMoreAction), for: .touchUpInside)

        return button
    }()

    lazy fileprivate var directorView: DetailsSubcontainerView! = {
        return DetailsSubcontainerView()
    }()
    
    lazy fileprivate var actorView: DetailsSubcontainerView! = {
        return DetailsSubcontainerView()
    }()
    
    lazy fileprivate var actorDisplayInfoView: DetailsSubcontainerView! = {
        return DetailsSubcontainerView()
    }()
    
    lazy fileprivate var stackView: UIStackView = {
        let sView = UIStackView()
        sView.axis = .vertical
        sView.distribution = .fillEqually
        sView.alignment = .fill
        sView.spacing = 8
        
        return sView
    }()
    
    // MARK: Life cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        configureView()
        self.presenter.updateView()
    }

    func configureView() {
        view.backgroundColor = .white
        setupStack()
        setupButton()
        
        setExpanded(expanded: false)        
    }
    
    func setupStack() {
        // Setup – Director view
        view.addSubview(directorView)
        directorView.mas_makeConstraints { (make:MASConstraintMaker?) in
            make?.top.equalTo()(view.mas_topMargin)?.with().offset()(view.layoutMargins.top+16)
            make?.left.equalTo()(view.mas_left)
            make?.right.equalTo()(view.mas_right)
        }
        
        // Setup – Stack view
        stackView.addArrangedSubview(actorView)
        stackView.addArrangedSubview(actorDisplayInfoView)
        view.addSubview(stackView)
        stackView.mas_makeConstraints { (make:MASConstraintMaker?) in
            make?.top.equalTo()(directorView.mas_bottom)?.with().offset()(32)
            make?.left.equalTo()(view.mas_left)
            make?.right.equalTo()(view.mas_right)
        }
    }
    
    func setupButton() {
        // Setup – Tappable label
        view.addSubview(showMoreButton)
        showMoreButton.mas_makeConstraints { (make:MASConstraintMaker?) in
            make?.top.equalTo()(directorView.mas_bottom)?.with().offset()(32)
            make?.left.equalTo()(directorView.mas_left)?.with().offset()(16)
            make?.right.equalTo()(directorView.mas_right)
            make?.height.equalTo()(50)
        }
    }
    
    func setExpanded(expanded: Bool) {
        actorView.isHidden = expanded == false
        actorDisplayInfoView.isHidden = expanded == false
        showMoreButton.isHidden = expanded == true
    }
    
    func showMoreAction() {
        setExpanded(expanded: true)
    }
}

extension DetailsViewController: DetailsViewInput {

    func didRefreshFilm() {
        let film = self.presenter.film
        let actor: Actor? = film?.cast[0]
        
        directorView.title = NSLocalizedString("Director Name", comment: "Director name")
        directorView.text = film?.director.name
        
        actorView.title = NSLocalizedString("Actor Name", comment: "Actor name")
        actorView.text = actor?.name
        
        actorDisplayInfoView.title = NSLocalizedString("Actor screen Name", comment: "Actor screen name")
        actorDisplayInfoView.text = actor?.screenName
        
        self.view.setNeedsUpdateConstraints()
        self.view.updateConstraintsIfNeeded()
    }
}







