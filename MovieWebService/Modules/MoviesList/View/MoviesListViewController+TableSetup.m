//
//  MoviesListViewController+TableSetup.m
//  MovieWebService
//
//  Created by Vitaliy Zarubin on 08.11.17.
//  Copyright © 2017 TestCompany. All rights reserved.
//

#import "MoviesListViewController+TableSetup.h"

// Cells
#import "FilmTableViewCell.h"

@implementation MoviesListViewController (TableSetup)

- (void)setup {
    UINib *lastBetNib = [UINib nibWithNibName:NSStringFromClass([FilmTableViewCell class]) bundle:nil];
    [self.tableView registerNib:lastBetNib forCellReuseIdentifier:FilmTableViewCellIdentifier];
}

@end
