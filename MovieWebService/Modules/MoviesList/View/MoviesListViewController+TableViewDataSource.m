//
//  MoviesListViewController+TableViewDataSource.m
//  MovieWebService
//
//  Created by Vitaliy Zarubin on 08.11.17.
//  Copyright © 2017 TestCompany. All rights reserved.
//

#import "MoviesListViewController+TableViewDataSource.h"
// Cells
#import "FilmTableViewCell.h"

@implementation MoviesListViewController (TableViewDataSource)

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.presenter filmsCount];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FilmTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:FilmTableViewCellIdentifier forIndexPath:indexPath];
    cell.film = [self.presenter filmAtIndex:indexPath.row];
    
    return cell;
}

@end
