//
//  MoviesListViewController+TableSetup.h
//  MovieWebService
//
//  Created by Vitaliy Zarubin on 08.11.17.
//  Copyright © 2017 TestCompany. All rights reserved.
//

#import "MoviesListViewController.h"

@interface MoviesListViewController (TableSetup)

- (void)setup;
@end
