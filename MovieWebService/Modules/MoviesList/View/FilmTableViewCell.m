//
//  FilmTableViewCell.m
//  MovieWebService
//
//  Created by testDev on 4/11/17.
//  Copyright © 2017 TestCompany. All rights reserved.
//

#import "FilmTableViewCell.h"

// Models
#import "Film.h"

// Utils
#import "NSDateFormatter+SharedFormatters.h"

float const FilmTableViewCellHeight = 60;
NSString * const FilmTableViewCellIdentifier = @"FilmTableViewCell";

@interface FilmTableViewCell()
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *filmRatingNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *markLabel;
@end

@implementation FilmTableViewCell
#pragma mark - Lifecycle
- (void)awakeFromNib {
    [super awakeFromNib];
}

#pragma mark - Setter/getter
- (void)setFilm:(Film *)film {
    _film = film;
    
    self.nameLabel.text = film.name;
    self.dateLabel.text = [[NSDateFormatter simpleDateTimeFormatter] stringFromDate:film.releaseDate];
    self.filmRatingNameLabel.text = film.filmRatingName;
    self.markLabel.text = [@(film.mark) stringValue];
}
@end
