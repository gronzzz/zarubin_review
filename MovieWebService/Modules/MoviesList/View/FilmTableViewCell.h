//
//  FilmTableViewCell.h
//  MovieWebService
//
//  Created by testDev on 4/11/17.
//  Copyright © 2017 TestCompany. All rights reserved.
//

#import <UIKit/UIKit.h>

FOUNDATION_EXPORT float const FilmTableViewCellHeight;
FOUNDATION_EXPORT NSString * const FilmTableViewCellIdentifier;

@class Film;
@interface FilmTableViewCell : UITableViewCell

@property (nonatomic, strong) Film *film;
@end
