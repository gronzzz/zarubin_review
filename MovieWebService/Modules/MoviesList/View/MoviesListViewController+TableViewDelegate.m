//
//  MoviesListViewController+TableViewDelegate.m
//  MovieWebService
//
//  Created by Vitaliy Zarubin on 08.11.17.
//  Copyright © 2017 TestCompany. All rights reserved.
//

#import "MoviesListViewController+TableViewDelegate.h"
// Cells
#import "FilmTableViewCell.h"

@implementation MoviesListViewController (TableViewDelegate)

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return FilmTableViewCellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.presenter didSelectItemAtIndex:indexPath.row];
}

@end
