//
//  MoviesListViewController.m
//  MovieWebService
//
//  Created by testDev on 11/04/2017.
//  Copyright © 2017 Agoda Services Co. Ltd. All rights reserved.
//

#import "MoviesListViewController.h"

#import "MoviesListView.h"
#import "Film.h"
#import "MoviesListViewController+TableSetup.h"


@implementation MoviesListViewController

#pragma mark - Life cycle

- (void)viewDidLoad {
	[super viewDidLoad];
    [self configureView];
    [self.presenter updateView];
}

- (void)configureView {
    self.navigationItem.title = @"RootViewController";
    [self setup];
}


#pragma mark - MoviesListViewOutput

- (void)didRefreshFilms {
    [self.tableView reloadData];
}

@end

