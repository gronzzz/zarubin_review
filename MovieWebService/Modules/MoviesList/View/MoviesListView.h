//
//  MoviewsListView.h
//  MovieWebService
//
//  Created by Vitaliy Zarubin on 15.10.17.
//  Copyright © 2017 TestCompany. All rights reserved.
//

@protocol MoviesListViewOutput <NSObject>
- (void)didRefreshFilms;
@end
