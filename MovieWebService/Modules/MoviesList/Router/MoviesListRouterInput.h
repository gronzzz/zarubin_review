//
//  MoviesListRouterInput.h
//  MovieWebService
//
//  Created by testDev on 11/04/2017.
//  Copyright © 2017 Agoda Services Co. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MoviesListView.h"

@class Film;
@protocol MoviesListRouterInput <NSObject>

- (void)presentDetailsViewControllerWithFilm:(Film *)film;

@end
