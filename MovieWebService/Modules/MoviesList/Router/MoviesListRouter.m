//
//  MoviesListRouter.m
//  MovieWebService
//
//  Created by testDev on 11/04/2017.
//  Copyright © 2017 Agoda Services Co. Ltd. All rights reserved.
//

#import "MoviesListRouter.h"
#import "AssemblyBuilder.h"

@implementation MoviesListRouter

#pragma mark - MoviesListRouterInput

- (void)presentDetailsViewControllerWithFilm:(Film *)film {
    NSAssert(self.view, @"There is no view in MoviesListRouter that belongs to MoviesListViewOutput protocol");
    
    UIViewController *currentVC = (UIViewController *)self.view;
    UIViewController *targetVC = [AssemblyBuilder viewForMovieDetailsWithFilm:film];
    [currentVC.navigationController pushViewController:targetVC animated:YES];
}

@end


