//
//  MoviesListPresenter.m
//  MovieWebService
//
//  Created by testDev on 11/04/2017.
//  Copyright © 2017 Agoda Services Co. Ltd. All rights reserved.
//

#import "MoviesListPresenter.h"

// Models
#import "Film.h"

@interface MoviesListPresenter()
@property (nonatomic, weak, nullable, readwrite) id<MoviesListViewOutput> view;
@property (nonatomic, strong, readwrite) id<MoviesListInteractorInput> interactor;
@property (nonatomic, strong, readwrite) id<MoviesListRouterInput> router;

@property (nonatomic, strong) NSArray *films;
@end

@implementation MoviesListPresenter

#pragma mark - Lifecycle

- (instancetype)initWithInterface:(id<MoviesListViewOutput>)view
                       interactor:(id<MoviesListInteractorInput>)interactor
                           router:(id<MoviesListRouterInput>)router {
    self = [super init];
    if (self) {
        _view = view;
        _interactor = interactor;
        _router = router;
    }
    
    return self;
}

#pragma mark - Public

- (void)updateView {
    [self.interactor getFilms];
}

- (NSUInteger)filmsCount {
    return [self.films count];
}

- (Film *)filmAtIndex:(NSInteger)index {
    return self.films[index];
}

- (void)didSelectItemAtIndex:(NSInteger)index {
    Film *film = [self filmAtIndex:index];
    [self.router presentDetailsViewControllerWithFilm:film];
}

#pragma mark - MoviesListViewOutput

#pragma mark - MoviesListInteractorOutput

- (void)didLoadFilms:(NSArray *)films {
    self.films = films;
    dispatch_sync(dispatch_get_main_queue(), ^(void) {
        [self.view didRefreshFilms];
    });
}
@end
