//
//  MoviesListPresenterInput.h
//  MovieWebService
//
//  Created by Vitaliy Zarubin on 16.10.17.
//  Copyright © 2017 TestCompany. All rights reserved.
//

@protocol MoviesListPresenterInput <NSObject>

- (void)updateView;
- (NSUInteger)filmsCount;
- (Film *)filmAtIndex:(NSInteger)index;

- (void)didSelectItemAtIndex:(NSInteger)index;

@end
