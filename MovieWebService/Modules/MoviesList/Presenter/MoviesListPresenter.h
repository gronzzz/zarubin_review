//
//  MoviesListPresenter.h
//  MovieWebService
//
//  Created by testDev on 11/04/2017.
//  Copyright © 2017 Agoda Services Co. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "MoviesListView.h"
#import "MoviesListInteractorIO.h"
#import "MoviesListRouterInput.h"
#import "MoviesListPresenterInput.h"

NS_ASSUME_NONNULL_BEGIN

@interface MoviesListPresenter : NSObject <MoviesListInteractorOutput, MoviesListPresenterInput>

@property (nonatomic, weak, nullable, readonly) id<MoviesListViewOutput> view;
@property (nonatomic, strong, readonly) id<MoviesListInteractorInput> interactor;
@property (nonatomic, strong, readonly) id<MoviesListRouterInput> router;

- (instancetype)initWithInterface:(id<MoviesListViewOutput>)view
                       interactor:(id<MoviesListInteractorInput>)interactor
                           router:(id<MoviesListRouterInput>)router;

@end

NS_ASSUME_NONNULL_END
