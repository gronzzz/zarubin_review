//
//  MoviesListInteractorIO.h
//  MovieWebService
//
//  Created by Vitaliy Zarubin on 15.10.17.
//  Copyright © 2017 TestCompany. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MoviesListInteractorOutput <NSObject>
- (void)didLoadFilms:(NSArray *)films;
@end

@class DBManager;
@protocol MoviesListInteractorInput <NSObject>
- (void)getFilms;
- (DBManager *)getDBManager;

- (void)anyTestProtocolMethod;
@end


