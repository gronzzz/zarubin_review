//
//  MoviesListInteractor.m
//  MovieWebService
//
//  Created by testDev on 11/04/2017.
//  Copyright © 2017 Agoda Services Co. Ltd. All rights reserved.
//

#import "MoviesListInteractor.h"

#import "MoviesListInteractorIO.h"
#import "Film.h"
#import "DBManager.h"


@implementation MoviesListInteractor

#pragma mark - MoviesListInteractorInput

- (void)getFilms {
    __weak typeof(self) wself = self;
    [[DBManager sharedInstance] getFilmWithCallback:^(Film *film) {
        [wself.output didLoadFilms:@[film]];
    }];
}

- (DBManager *)getDBManager {
    return [DBManager sharedInstance];
}

#pragma mark – Test
- (void)anyTestProtocolMethod {
    [[DBManager sharedInstance] testJsonMethod];
}

@end
