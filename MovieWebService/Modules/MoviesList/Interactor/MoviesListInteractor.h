//
//  MoviesListInteractor.h
//  MovieWebService
//
//  Created by testDev on 11/04/2017.
//  Copyright © 2017 Agoda Services Co. Ltd. All rights reserved.
//

#import "MoviesListInteractorIO.h"

NS_ASSUME_NONNULL_BEGIN

@class Film;
@interface MoviesListInteractor : NSObject <MoviesListInteractorInput>

@property (nonatomic, weak, nullable) id<MoviesListInteractorOutput> output;
@end

NS_ASSUME_NONNULL_END
