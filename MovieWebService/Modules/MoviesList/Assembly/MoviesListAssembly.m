//
//  MoviesListAssembly.m
//  MovieWebService
//
//  Created by testDev on 11/04/2017.
//  Copyright © 2017 Agoda Services Co. Ltd. All rights reserved.
//

#import "MoviesListAssembly.h"

#import "MoviesListViewController.h"
#import "MoviesListInteractor.h"
#import "MoviesListPresenter.h"
#import "MoviesListRouter.h"

@implementation MoviesListAssembly

+ (UIViewController *)build {
    MoviesListViewController *view = [[MoviesListViewController alloc] initWithNibName:NSStringFromClass([MoviesListViewController class]) bundle:nil];
    MoviesListRouter *router = [[MoviesListRouter alloc] init];

    MoviesListInteractor *interactor = [[MoviesListInteractor alloc] init];
    MoviesListPresenter *presenter = [[MoviesListPresenter alloc] initWithInterface:view
                                                                         interactor:interactor
                                                                             router:router];
    
    interactor.output = presenter;
    
    view.presenter = presenter;
    
    router.view = view;
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:view];
    navigationController.navigationBar.translucent = NO;
    
    return navigationController;
}

@end
