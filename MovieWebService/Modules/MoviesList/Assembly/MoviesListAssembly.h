//
//  MoviesListAssembly.h
//  MovieWebService
//
//  Created by testDev on 11/04/2017.
//  Copyright © 2017 Agoda Services Co. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoviesListAssembly: NSObject

+ (UIViewController *)build;

- (instancetype) init __attribute__((unavailable("init not available")));

@end
